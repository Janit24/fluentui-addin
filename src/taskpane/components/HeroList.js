import * as React from "react";
import PropTypes from "prop-types";
import { Button } from "office-ui-fabric-react";

export default class HeroList extends React.Component {

  bookRoom = async () => {
    Office.context.mailbox.item.location.getAsync(function (asyncResult) {
      if (asyncResult.status == Office.AsyncResultStatus.Failed) {
        console.log(asyncResult.error.message);
      } else {
        // Successfully got the location, display it.
        console.log("The location is: " + asyncResult.value);
      }
    });
  };

  render() {
    const { children, items, message } = this.props;

    const listItems = items.map((item, index) => (
      <>
        <li className="ms-ListItem" key={index}>
          <i className={`ms-Icon ms-Icon--${item.email}`}></i>
          <span className="ms-font-m ms-fontColor-neutralPrimary">{item.roomName}</span>
          <Button className="book" onClick={(e) => this.bookRoom(e)}>Book</Button>
        </li>
      </>
    ));
    return (
      <main className="ms-welcome__main">
        <h2 className="ms-font-xl ms-fontWeight-semilight ms-fontColor-neutralPrimary ms-u-slideUpIn20">{message}</h2>
        <ul className="ms-List ms-welcome__features ms-u-slideUpIn10">{listItems.length > 0 ? listItems: ""}</ul>
        {children}
      </main>
    );
  }
}

HeroList.propTypes = {
  children: PropTypes.node,
  items: PropTypes.array,
  message: PropTypes.string,
};
