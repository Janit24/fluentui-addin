import * as React from "react";
import PropTypes from "prop-types";
import { Button, ButtonType, Nav } from "office-ui-fabric-react";
import Header from "./Header";
import HeroList from "./HeroList";
import Progress from "./Progress";

export default class App extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      listItems: [],
      hasData: false,
    };
  }

  componentDidMount() {
    this.setState({
      listItems: [
        {
          icon: "Ribbon",
          primaryText: "Achieve more with Office integration",
        },
        {
          icon: "Unlock",
          primaryText: "Unlock features and functionality",
        },
        {
          icon: "Design",
          primaryText: "Create and visualize like a pro",
        },
      ],
    });
  }

  click = async () => {
    /**
     * Insert your Outlook code here
     */
    // eslint-disable-next-line no-undef
    // let data = await (await fetch("https://jsonplaceholder.typicode.com/users/")).json();
    // if (Array.isArray(data) && data.length > 0) {
    //   this.setState({ listItems: data, hasData: true });
    // }
    await this.getRooms();
  };

  getRooms = async () => {
    let roomResponse = await fetch(
      "https://scdeventerprise.azurewebsites.net/webapi/minisync/rooms?location=%2FIndia%2FG%20(0)%2FUP_NCR%2FNoida%20renamed%2FG%20(0)%2FMeeting%20Room%2FSelf%20Managed%2F&fromDateTime=2021-09-28T09%3A30%3A00Z&toDateTime=2021-09-28T10%3A00%3A00Z&roomProviderTypes=Exchange&capacity=1&rowCount=0&noOfRecords=10&attributeIds=%2C&culture=en-GB&t=1632727461352",
      {
        method: "GET",
        headers: {
          Authorization:
            "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJodHRwOi8vd3d3LnczLm9yZy8yMDAxLzA0L3htbGRzaWctbW9yZSNobWFjLXNoYTUxMiJ9.eyJpZCI6IjFiNDExZWFiLWU3OGEtNGEyOC04MWM3LTc2M2RhNzUxMjUzYSIsImNvbXBvbmVudCI6Ik91dGxvb2siLCJ1c2VybmFtZSI6Ikphbml0R2lyaSIsInJvbGUiOiJ1c2VyIiwiaXNzIjoiQ29uZGVjbyIsImF1ZCI6ImUzYjEwNTJjYzJmMTRlNTU4YTNiMWQ0Mzc4NGE0ODJlIiwiZXhwIjoxNjMyNzkyMDk2LCJuYmYiOjE2MzI3NDg4OTZ9.AUbZE_9yJ2Ga3Ud0KjphWmsnKnIdJrAzIWKoQbPxYgTlwFo7MccnXNoULuCcYS_a7ZMVanuP0J_7gR2Xyx5-gw",
        },
      }
    );
    if (roomResponse.status == 200) {
      let data = await roomResponse.json();
      this.setState({ listItems: data.rooms, hasData: true });
    }
  }

  render() {
    const { title, isOfficeInitialized } = this.props;

    if (!isOfficeInitialized) {
      return (
        <Progress title={title} logo="assets/logo-filled.png" message="Please sideload your addin to see app body." />
      );
    }

    return (
      <div className="ms-welcome">
        {!this.state.hasData && <Header logo="assets/logo-filled.png" title={this.props.title} message="Welcome" />}
        {this.state.hasData && <HeroList message="Condeco Add-In" items={this.state.listItems}>
            <p className="ms-font-l">{/* Modify the source files, then click <b>Run</b>. */}</p>
          </HeroList>
        }
        <center>
          <Button
            className="ms-welcome__action"
            buttonType={ButtonType.hero}
            iconProps={{ iconName: "ChevronRight" }}
            onClick={this.click}
          >
            Search Rooms
          </Button>
        </center>
      </div>
    );
  }
}

App.propTypes = {
  title: PropTypes.string,
  isOfficeInitialized: PropTypes.bool,
};
